<form method="POST" id="product_form">
    <div class="page-header">
        <p class="header-title">Create new Product</p>
        <div class="header-buttons">
            <button type="submit" class="btn btn-primary" id="saveButton">Save</button>
            <a href="/products" class="btn btn-light">Cancel</a>
        </div>
    </div>
    <hr>
    <?php if (!empty($errors)) : ?>
        <div class="alert alert-danger">
            <?php foreach ($errors as $error) : ?>
                <div><?php echo $error ?></div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
    <div class="input-style">
        <label>SKU</label>
        <input type="text" class="form-control" id="sku" name="SKU" value="<?php echo $productData['SKU'] ?>">
    </div>
    <p id="skuCheck" class="validator">
        SKU is REQUIRED
    </p>
    <div class="input-style">
        <label>Name</label>
        <input type="text" class="form-control" id="name" name="Name" value="<?php echo $productData['Name'] ?>">
    </div>
    <p id="nameCheck" class="validator">
        Name is REQUIRED
    </p>
    <div class="input-style">
        <label>Price</label>
        <input type="number" step=".01" class="form-control" id="price" name="Price" value="<?php echo $productData['Price'] ?>">
    </div>
    <p id="priceCheck" class="validator">
        Price is REQUIRED
    </p>
    <select class="dropdown" id="productType" name="type">
        <option class="dropdown-item" value="" id="default" <?php if (($_POST['type'] ?? '') == '') : ?> selected <?php endif ?>>Please Select a type</option>
        <option class="dropdown-item" value="DVD" id="DVD" <?php if (($_POST['type'] ?? '') == 'DVD') : ?> selected <?php endif ?>>DVD</option>
        <option class="dropdown-item" value="Book" id="Book" <?php if (($_POST['type'] ?? '') == 'Book') : ?> selected <?php endif ?>>Book</option>
        <option class="dropdown-item" value="Furniture" id="Furniture" <?php if (($_POST['type'] ?? '') == 'Furniture') : ?> selected <?php endif ?>>Furniture</option>
    </select>
    <p id="typeCheck" class="validator">
        Type is REQUIRED!
    </p>
    <div class="input-style" id="inputSize" style="display: none">
        <label>Size (in MB)</label>
        <input type="number" class="form-control" name="Size" id="size" value="<?php echo $productData['Size'] ?>">
    </div>
    <p id="sizeCheck" class="validator">
        Size is REQUIRED
    </p>
    <div class="input-style" id="inputWeight" style="display: none">
        <label>Weight (in KG)</label>
        <input type="number" step=".01" class="form-control" id="weight" name="Weight" value="<?php echo $productData['Weight'] ?>">
    </div>
    <p id="weightCheck" class="validator">
        Weight is REQUIRED
    </p>
    <div class="input-style" id="inputHeight" style="display: none">
        <label>Height (in M)</label>
        <input type="number" step=".01" class="form-control" id="height" name="Height" value="<?php echo $productData['Height'] ?>">
    </div>
    <p id="heightCheck" class="validator">
        Height is REQUIRED
    </p>
    <div class="input-style" id="inputWidth" style="display: none">
        <label>Width (in M)</label>
        <input type="number" step=".01" class="form-control" id="width" name="Width" value="<?php echo $productData['Width'] ?>">
    </div>
    <p id="widthCheck" class="validator">
        Width is REQUIRED
    </p>
    <div class="input-style" id="inputLength" style="display: none">
        <label>Length (in M)</label>
        <input type="number" step=".01" class="form-control" id="length" name="Length" value="<?php echo $productData['Length'] ?>">
    </div>
    <p id="lengthCheck" class="validator">
        Length is REQUIRED
    </p>
</form>
<script src="/selectorHandler.js"></script>
<script src="/validation.js"></script>