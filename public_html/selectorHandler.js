$(document).ready(function () {
    $selectedElement = $("#productType").val();
    hideAllExcept($selectedElement);
});

$("#productType").on("change",
    function () {
        var type = $(this).val();
        if (type == "DVD") {
            hideAllExcept('DVD');
        } else if (type == "Book") {
            hideAllExcept('Book');
        } else if (type == "Furniture") {
            hideAllExcept('Furniture');
        }
    }
);

function hideAllExcept($element) {
    $allElements = {
        'DVD': ['inputSize'],
        'Book': ['inputWeight'],
        'Furniture': ['inputHeight', 'inputWidth', 'inputLength'],
    };

    $.each($allElements, function (index, value) {
        if (index == $element) {
            $.each(value, function (i, val) {
                $(`#${val}`).show();
            });
        } else {
            $.each(value, function (i, val) {
                $(`#${val}`).find("input").val('');
                $(`#${val}`).hide();

            });
        }
    });
}