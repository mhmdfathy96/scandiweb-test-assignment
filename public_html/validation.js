
$(document).ready(
    function () {
        let skuError = validationValue("sku", "skuCheck");
        let nameError = validationValue("name", "nameCheck");
        let priceError = validationValue("price", "priceCheck");
        hideAll();
        let sizeError;
        let weightError;
        let heightError;
        let widthError;
        let lengthError;

        var selectedOption;
        $("#productType").on("change",
            function () {
                hideAll();
                selectedOption = $(this).val();

                if (selectedOption == "DVD") {
                    weightError = validationValue("size", "sizeCheck");
                } else if (selectedOption == "Book") {
                    weightError = validationValue("weight", "weightCheck");
                } else if (selectedOption == "Furniture") {
                    heightError = validationValue("height", "heightCheck");
                    widthError = validationValue("width", "widthCheck");
                    lengthError = validationValue("length", "lengthCheck");
                }
            }
        );


        // Submit button
        $('#saveButton').click(function () {
            selectedOption = $("#productType").val();
            $("#productType").val(selectedOption).select();
            skuError = validation("sku", "skuCheck");
            nameError = validation("name", "nameCheck");
            priceError = validation("price", "priceCheck");
            if (selectedOption == null || selectedOption == 'Please Select a type' || selectedOption == '') {
                $('#typeCheck').show();
            }
            if (selectedOption == 'DVD') {
                sizeError = validation("size", "sizeCheck");
            }
            if (selectedOption == 'Book') {
                weightError = validation("weight", "weightCheck");
            }
            if (selectedOption == 'Furniture') {
                heightError = validation("height", "heightCheck");
                widthError = validation("width", "widthCheck");
                lengthError = validation("length", "lengthCheck");
            }
            console.log(sizeError, selectedOption);
            if ((skuError == true) && (nameError == true) && (priceError == true) && (selectedOption == 'DVD') && (sizeError == true)) {
                return true;
            } else if ((skuError == true) && (nameError == true) && (priceError == true) && (selectedOption == 'Book') && (weightError == true)) {
                return true;
            } else if ((skuError == true) && (nameError == true) && (priceError == true) && (selectedOption == 'Furniture') && (heightError == true) && (widthError == true) && (lengthError == true)) {
                return true;
            } else {
                return false;
            }

        });

        function hideAll() {
            $allElements = ['typeCheck', "sizeCheck", "weightCheck", "heightCheck", "widthCheck", "lengthCheck"];

            $.each($allElements, function (index, value) {
                $(`#${value}`).hide();
            });
        }

        function validationValue(inputID, checkID) {
            $(`#${checkID}`).hide();
            $(`#${inputID}`).keyup(
                function () {
                    return validation(inputID, checkID);
                }
            );
        }

        function validation(inputID, checkID) {
            let thisValue = $(`#${inputID}`).val();
            if (thisValue == '' || thisValue == 0) {
                $(`#${checkID}`).show();
                return false;
            } else {
                $(`#${checkID}`).hide();
                return true;
            }
        }
    }
);