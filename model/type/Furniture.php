<?php

namespace app\model\type;

use app\model\iSpecificProps\HavingHeight;
use app\model\iSpecificProps\HavingLength;
use app\model\iSpecificProps\HavingWidth;
use app\model\mainProduct\Product;
use app\model\tSpecificProps\WithHeight;
use app\model\tSpecificProps\WithLength;
use app\model\tSpecificProps\WithWidth;

class Furniture extends Product implements HavingHeight, HavingWidth, HavingLength
{
    use WithHeight;
    use WithWidth;
    use WithLength;
}
