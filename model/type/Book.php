<?php

namespace app\model\type;

use app\model\iSpecificProps\HavingWeight;
use app\model\mainProduct\Product;
use app\model\tSpecificProps\WithWeight;


class Book extends Product implements HavingWeight
{

    use WithWeight;

}