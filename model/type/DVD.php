<?php
namespace app\model\type;

use app\model\iSpecificProps\HavingSize;
use app\model\mainProduct\Product;
use app\model\tSpecificProps\WithSize;

class DVD extends Product implements HavingSize
{
    use WithSize;
}