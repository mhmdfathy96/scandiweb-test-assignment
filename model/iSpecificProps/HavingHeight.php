<?php

namespace app\model\iSpecificProps;

interface HavingHeight
{

    public function setHeight($Height);
    public function getHeight();

}