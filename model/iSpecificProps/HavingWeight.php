<?php

namespace app\model\iSpecificProps;

interface HavingWeight
{

    public function setWeight($Weight);
    public function getWeight();

}