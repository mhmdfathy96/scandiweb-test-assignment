<?php

namespace app\model\iSpecificProps;

interface HavingLength
{

    public function setLength($Length);
    public function getLength();

}