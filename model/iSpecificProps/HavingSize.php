<?php

namespace app\model\iSpecificProps;

interface HavingSize
{

    public function setSize($Size);
    public function getSize();

}