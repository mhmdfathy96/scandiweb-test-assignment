<?php

namespace app\model\iSpecificProps;

interface HavingWidth
{

    public function setWidth($Width);
    public function getWidth();

}