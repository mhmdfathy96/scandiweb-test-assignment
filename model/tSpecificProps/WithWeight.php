<?php

namespace app\model\tSpecificProps;

trait WithWeight
{
    private $Weight;

    /**
     * @return mixed
     */
    public function getWeight()
    {
        return $this->Weight;
    }

    /**
     * @param mixed $Weight
     */
    public function setWeight($Weight)
    {
        $this->Weight = $Weight;
    }



}