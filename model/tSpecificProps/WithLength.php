<?php

namespace app\model\tSpecificProps;

trait WithLength
{
    private $Length;

    /**
     * @return mixed
     */
    public function getLength()
    {
        return $this->Length;
    }

    /**
     * @param mixed $Length
     */
    public function setLength($Length)
    {
        $this->Length = $Length;
    }



}