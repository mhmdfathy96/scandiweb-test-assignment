<?php

namespace app\model\tSpecificProps;

trait WithHeight
{
    private $Height;

    /**
     * @return mixed
     */
    public function getHeight()
    {
        return $this->Height;
    }

    /**
     * @param mixed $Height
     */
    public function setHeight($Height)
    {
        $this->Height = $Height;
    }



}