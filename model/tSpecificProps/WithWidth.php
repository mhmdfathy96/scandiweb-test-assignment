<?php

namespace app\model\tSpecificProps;

trait WithWidth
{
    private $Width;

    /**
     * @return mixed
     */
    public function getWidth()
    {
        return $this->Width;
    }

    /**
     * @param mixed $Width
     */
    public function setWidth($Width)
    {
        $this->Width = $Width;
    }



}