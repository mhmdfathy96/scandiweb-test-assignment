<?php

namespace app\model\tSpecificProps;

trait WithSize
{
    private $Size;

    /**
     * @return mixed
     */
    public function getSize()
    {
        return $this->Size;
    }

    /**
     * @param mixed $Size
     */
    public function setSize($Size)
    {
        $this->Size = $Size;
    }



}