<?php

namespace app\controllers;

use \app\controllers\IController;
use \app\bussinessLayer\ProductBussinessLayer;

class ProductController implements IController
{
    private $pbl;
    public function __construct()
    {
            $this->pbl=new ProductBussinessLayer();
    }

    public function index()
    {
        $router = Router::$router;
        $products = $this->pbl->fetchObjects();
        $router->renderView("/index", [
            'products' => $products,
        ]);
    }

    public function create()
    {
        $router = Router::$router;
        $errors = [];
        $productData = [
            'SKU' => '',
            'Name' => '',
            'Price' => '',
            'type'=>'',
            'Size' => '',
            'Weight' => '',
            'Height' => '',
            'Width' => '',
            'Length' => '',
        ];
        $type='';
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $productData['SKU'] = $_POST['SKU'];
            $productData['Name'] = $_POST['Name'];
            $productData['Price'] = (float)$_POST['Price'];
            $productData['type']=$_POST['type'];
            $productData['Size'] = $_POST['Size'] ?? null;
            $productData['Weight'] = $_POST['Weight'] ?? null;
            $productData['Height'] = $_POST['Height']?? null;
            $productData['Width'] = $_POST['Width']?? null;
            $productData['Length'] = $_POST['Length']?? null;
            $type=$_POST['type']??null;
            //////////
            $errors=$this->pbl->addObject($productData);
            if (empty($errors)) {
                header('location: /products');
                exit();
            }
        }
        $router->renderView("/create", [
            'productData' => $productData,
            'errors' => $errors,
            'type'=>$type,
        ]);
    }

    public function delete()
    {
        $products = $this->pbl->fetchObjects();
        foreach ($products as $product) {
            $checked = isset($_POST[$product->getSKU()]);
            if ($checked) $this->pbl->deleteObject($product->getSKU());
        }
        header('Location: /products');
    }
}
