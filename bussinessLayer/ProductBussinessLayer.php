<?php

namespace app\bussinessLayer;

use app\bussinessLayer\Database\productDatabase;
use app\model\type\Book;
use app\model\type\DVD;
use app\model\type\Furniture;
use app\model\mainProduct\Product;

class ProductBussinessLayer implements IBussinessLayer
{

    private $db;

    public function __construct()
    {
        $this->db = new productDatabase();
    }

    public function fetchObjects(): array
    {
        $productModels = [];
        $products = $this->db->fetchProducts();
        foreach ($products as $product) {
            $productModel = $this->fromJson($product);
            $productModels[] = $productModel;
        }
        return $productModels;
    }

    public function addObject($object)
    {
        $productModel = $this->fromJson($object);
        $isUniqueSKU = $this->db->isUniqueSKU($productModel->getSKU());
        if ($isUniqueSKU) {
            $this->db->addProduct($productModel);
        } else {
            return ["SKU_ALREADY_EXIST"];
        }
    }

    private function fromJson($productJson)
    {
        $productModel = null;
        if ($productJson['Size']!='') {
            $productModel = new DVD();
            $productModel->setSize($productJson['Size']);
        } else if ($productJson['Weight']!='') {
            $productModel = new Book();
            $productModel->setWeight($productJson['Weight']);
        } else if ($productJson['Height']!='') {
            $productModel = new Furniture();
            $productModel->setHeight($productJson['Height']);
            $productModel->setWidth($productJson['Width']);
            $productModel->setLength($productJson['Length']);
        }
        $productModel->setSKU($productJson['SKU']);
        $productModel->setName($productJson['Name']);
        $productModel->setPrice($productJson['Price']);
        $productModel->setDate(date("Y-m-d H:i"));
        return $productModel;
    }

    public function deleteObject($object)
    {
        return $this->db->deleteProduct($object);
    }
}