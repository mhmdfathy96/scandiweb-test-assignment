<?php

namespace app\bussinessLayer\Database;
use PDO;

class productDatabase
{
    protected $pdo;

    public function __construct()
    {
        $this->initConnection();
    }

    private function initConnection(){
        $host = 'localhost';
        $port = 3306;
        $dbName = "productdb"; //id18643217_productdb
        $username='root';    //dbuser //id18643217_dbuser
        $password='';          //S*s]~)<y?W&b<y9N
        $this->pdo = new PDO("mysql:host=$host;port=$port;dbname=$dbName", $username, $password);
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function fetchProducts()
    {
        $statement = $this->pdo->prepare("Select * from products order by date asc ");
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }
    public function isUniqueSKU($SKU): bool
    {
        $statement = $this->pdo->prepare('select * from products where SKU =:sku');
        $statement->bindValue(':sku', $SKU);
        $statement->execute();
        $product=$statement->fetch(PDO::FETCH_ASSOC);
        return !isset($product['SKU']);
    }
    public function addProduct($productModel)
    {
        $statement = $this->pdo->prepare("insert into products (SKU,Name,Price,Date,Size,Weight,Height,Width,Length) values (:sku,:name,:price,:date,:size,:weight,:height,:width,:length);");
        $statement->bindValue(':sku',$productModel->getSKU());
        $statement->bindValue(':name', $productModel->getName());
        $statement->bindValue(':price', $productModel->getPrice());
        $statement->bindValue(':date', date('Y-m-d H:i'));
        $statement->bindValue(':size', method_exists($productModel,'getSize')? $productModel->getSize():null);
        $statement->bindValue(':weight', method_exists($productModel,'getWeight')? $productModel->getWeight():null);
        $statement->bindValue(':height', method_exists($productModel,'getHeight')? $productModel->getHeight():null);
        $statement->bindValue(':width', method_exists($productModel,'getWidth')? $productModel->getWidth():null);
        $statement->bindValue(':length', method_exists($productModel,'getLength')? $productModel->getLength():null);
        $statement->execute();
       // return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    public function deleteProduct($SKU)
    {
        $statement = $this->pdo->prepare('delete from products where SKU =:sku');
        $statement->bindValue(':sku', $SKU);
        $statement->execute();
        //return $statement->fetchAll(PDO::FETCH_ASSOC);
    }
}
